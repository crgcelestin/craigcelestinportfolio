import {
    Box,
    Container
} from '@chakra-ui/react'
import ProjectHeader from '../components/projects/project-header';
import ThreeDObject from '../components/3d-object';
import NoSsr from '../components/no-ssr'

const Page = () => {
    return (
        <Container>
            <NoSsr>
                < ThreeDObject />
            </NoSsr>
            <Box>
                <ProjectHeader />
            </Box>
        </Container>
    )
}

export default Page;
