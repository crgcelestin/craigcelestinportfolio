import {
    Box,
    Container
} from '@chakra-ui/react'
import AboutMeHeader from '../components/about-me/aboutMeHeader'
import AboutMeEducation from '../components/about-me/aboutMeEducation'


const Page = () => {
    return (
        <>
            {/* <Box pt={5} /> */}
            <Container>
                <Box w="100%" my={2}>
                    <AboutMeHeader />
                </Box>
                <Box w="100%" my={2}>
                    <AboutMeEducation />
                </Box>
            </Container>
        </>
    )
}

export default Page
