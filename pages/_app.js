import { ChakraProvider } from "@chakra-ui/react";
import Layout from '../components/layouts/main'
import { useRouter } from "next/router";
import theme from "../lib/theme";
import Fonts from "../components/fonts";
import ScrollToTop from "../components/scrollTop";
import { AnimatePresence } from "framer-motion";


const Website = ({ Component, pageProps }) => {
    const router = useRouter()
    return (
        <ChakraProvider
            theme={theme}
        >
            <Fonts />
            <ScrollToTop />
            <Layout router={router}>
                <AnimatePresence
                    mode="wait"
                    inital={true}
                >
                    <Component key={router.asPath} {...pageProps} />
                </AnimatePresence>
            </Layout>

        </ChakraProvider>
    )
}

export default Website
