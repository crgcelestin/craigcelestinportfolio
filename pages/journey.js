import {
    Box,
    Container
} from '@chakra-ui/react'
import Journey from '../components/journey/journey'
import ThreeDObject from '../components/3d-object'
import NoSsr from '../components/no-ssr'

const Page = () => {
    return (
        <Container>
            <NoSsr>
                < ThreeDObject />
            </NoSsr>
            <Box pt={5}>
                <Journey />
            </Box>
        </Container>
    )
}


export default Page
