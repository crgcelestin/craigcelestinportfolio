- Implement AI Experience Page
    * Resume in pdf viewer
    * Each AI/ML Related Project with demo and repo link

```js
import { useState } from 'react'
import { Document } from "react-pdf";


const Page = () => {
    const [numPages, setNumPages] = useState(null);
    const [pageNumber, setPageNumber] = useState(1);
    function onDocumentLoadSuccess({ numPages }) {
        setNumPages(numPages);
    }
    return (
        <div>
            {/* <Document
                file="/files/Craig_Celestin_AI_Resume.pdf"
                onLoadSuccess={onDocumentLoadSuccess}
            >
                <Page pageNumber={pageNumber} />
            </Document>
            <p>
                Page {pageNumber} of {numPages}
            </p> */}
        </div>
    )
}

export default Page;
```
