import { ChakraProvider, Container, Box, Heading, Image, useColorModeValue, Text } from '@chakra-ui/react'
import NextLink from 'next/link'
import HookForm from '../components/home/contactform'
import Section from '../components/section'
import Bio from "../components/home/bio";
import '../components/styles/home.module.css'
import CurrentBio from "../components/home/currently";
import ActionButton from '../components/actionbutton';
import CurrentProjectsList from '../components/projects/currentProjects';
import Footer from '../components/footer';

const Page = () => {

    return (
        <>
            <Box pt={5} zIndex={2} />
            <Container zIndex={1}>
                <Box borderRadius="lg"
                    mb={6}
                    p={3}
                    textAlign="center"
                    bg={useColorModeValue('gray.400', 'whiteAlpha.200')}
                    zIndex={1}
                    css={{ backdropFilter: 'blur(10px)' }}>
                    Hello, I am an interdisciplinary software engineer with experience in AI Programming based in Brooklyn, NY.
                </Box>
                <Box display={{ md: 'flex' }}>
                    <Box flexGrow={1}>
                        <Heading as="h2" variant="page-title">
                            Craig Tyler Celestin
                        </Heading>
                        <p>
                            - Full Stack Software Engineer -
                        </p>
                        <Text sx={{ color: 'gray', fontSize: '14px', marginTop: '4px' }}>
                            Stay Striving
                        </Text>
                    </Box>
                    <Box
                        flexShrink={0}
                        mt={{ base: 4, md: 0 }}
                        ml={{ md: 6 }}
                        textAlign="center"
                    >
                        <Image
                            borderColor={useColorModeValue('gray.800', 'whiteAlpha.900')}
                            borderWidth={2}
                            borderStyle='solid'
                            display='inline-block'
                            borderRadius='full '
                            maxWidth='150px'
                            src='/images/CraigCelestin.jpeg'
                            alt='profile image'
                            bgGradient='linear(to-r, green.200, pink.500)'
                        />
                    </Box>
                </Box>
                <div style={{ marginTop: '50px' }} >
                    <Section delay={0.1}>
                        <Heading as='h3' variant='section-title'>
                            Bio
                        </Heading>
                        <Bio></Bio>
                        <Box>
                            <NextLink href={"/about-me"} passHref>
                                <ActionButton mt={0} marginTop="10px">
                                    <Text fontWeight="bold" size="sm">
                                        {"More about me →"}
                                    </Text>
                                </ActionButton>
                            </NextLink>
                        </Box>
                    </Section>
                </div>
                <div style={{ marginTop: '50px' }} >
                    <Section delay={0.1}>
                        <Heading as='h3' variant='section-title'>
                            At This Moment
                        </Heading>
                        <CurrentBio></CurrentBio>
                        <Box>
                            <NextLink href={"/journey"} passHref>
                                <ActionButton style={{ marginTop: '10px' }} mt={0}>
                                    <Text fontWeight="bold" size="sm">
                                        {"Full Work Experience →"}
                                    </Text>
                                </ActionButton>
                            </NextLink>
                        </Box>
                    </Section>
                </div>
                <div style={{ marginTop: '50px' }}>
                    <Section delay={0.1}>
                        <Heading as='h3' variant='section-title'>
                            Recent Projects
                        </Heading>
                        <CurrentProjectsList></CurrentProjectsList>
                        <Box>
                            <NextLink href={"/software-projects"} passHref>
                                <ActionButton style={{ marginTop: '10px' }} mt={0}>
                                    <Text fontWeight="bold" size="sm">
                                        {"Full Project Experience→"}
                                    </Text>
                                </ActionButton>
                            </NextLink>
                        </Box>
                        <Footer />
                    </Section>
                </div>
            </Container>
            <ChakraProvider>
                <div id='ContactForm'>
                    <Box p={100}>
                        <HookForm />
                    </Box>
                </div>
            </ChakraProvider >

        </>
    )
}

export default Page
