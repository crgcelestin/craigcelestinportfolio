import {
    Heading
} from '@chakra-ui/react'
import Section from '../section'
import CurrentBio from '../home/currently'
import PastDoings from '../home/pastdoing'


export default function Journey() {
    return (
        <div >
            <Section delay={0.1}>
                <Heading as='h3' fontSize="3xl" variant='section-title'>
                    What I am Currently Doing:
                </Heading>
                <CurrentBio />
                <Heading as='h3' fontSize="3xl" variant='section-title' style={{ marginTop: '30px' }}>
                    My Past Experiences:
                </Heading>
                <PastDoings />
            </Section>
        </div>
    )
}
