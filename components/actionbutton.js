import React from 'react';
import {
    Button,
    useColorModeValue,
} from '@chakra-ui/react';

const ActionButton = React.forwardRef((props, ref) => {
    return (
        <Button
            bg="transparent"
            fontSize="md"
            borderRadius={0}
            variant="link"
            pl={0}
            pr={3}
            py={3}
            ref={ref}
            w="min-content"
            border="2px solid transparent"
            color={useColorModeValue('gray.800', 'whiteAlpha.900')}
            fontWeight="regular"
            transition="0.35s ease-out"
            _hover={{
                border: "2px solid",
                pr: 3,
                pl: 3,
            }}
            {...props}
        >
            {props.children}
        </Button>
    );
});

ActionButton.displayName = 'ActionButton'
export default ActionButton;
