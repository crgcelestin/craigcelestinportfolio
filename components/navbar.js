import Logo from './logo'
import { forwardRef } from 'react'
import NextLink from 'next/link'
import {
    Container,
    Box,
    Link,
    Stack,
    Heading,
    Flex,
    Menu,
    MenuItem,
    MenuList,
    MenuButton,
    IconButton,
    useColorModeValue,
    useBreakpointValue
} from '@chakra-ui/react'
import { HamburgerIcon } from '@chakra-ui/icons'
import ThemeToggleButton from './theme-toggle-button'


const LinkItem = ({
    href, path, children, target, ...props
}) => {
    const active = path === href
    const inactiveColor = useColorModeValue('gray800', 'whiteAlpha.900')
    const fontSize = useBreakpointValue({ base: '15px', md: '15px' });
    return (
        <NextLink href={href} passHref>
            <Link
                as={NextLink}
                p={2}
                bg={active ? 'glassTeal' : undefined}
                color={active ? '#ffffff40' : inactiveColor}
                href={href}
                target={target}
                fontSize={fontSize}
                {...props}
            >
                {children}
            </Link>
        </NextLink>
    )
}

const MenuLink = forwardRef((props, ref) => (
    <Link
        ref={ref}
        as={NextLink}
        {...props}
    />
))
MenuLink.displayName = 'Menu Comp'
const NavBar = props => {
    const { path } = props
    return (
        <Box
            as="nav"
            position='absolute'
            w="100%"
            bg={useColorModeValue('#ffffff40', '#20202380')}
            style={{ backdropFilter: 'blur(30px)' }}
            zIndex={9}
            {...props}
        >
            <Container
                display="flex"
                p={2}
                maxW="container.md"
                wrap="wrap"
                align="center"
                justify="space-between"
            >
                <Flex align='center' mr={5}>
                    <Heading as="h1" size="lg" letterSpacing={'tighter'}>
                        <Logo />
                    </Heading>
                </Flex>
                <Stack
                    direction={{ base: 'column', md: 'row' }}
                    display={{ base: 'none', md: 'flex' }}
                    width={{ base: 'full', md: 'auto' }}
                    alignItems="center"
                    flexGrow={1}
                    overflowX="auto" // Use overflowX instead of flexWrap
                    mt={{ base: 4, md: 0 }}
                >
                    <LinkItem href='/about-me'
                        path={path}>
                        About Me
                    </LinkItem>
                    <LinkItem href='/journey'
                        path={path}>
                        Journey
                    </LinkItem>
                    <LinkItem href='/software-projects'
                        path={path}>
                        Software Projects
                    </LinkItem>
                    <LinkItem
                        href={"/files/Craig_Celestin_Resume.pdf"}
                        target="_blank"
                        rel="noopener noreferrer"
                        path={path}
                    >
                        Resume
                    </LinkItem>
                    <LinkItem target="_blank"
                        rel="noopener noreferrer"
                        href="/files/Craig_Celestin_AI_Resume.pdf"
                        path={path}>
                        AI Experience
                    </LinkItem>
                </Stack>
                <Box
                    flex={1}
                    align="right"
                >
                    <ThemeToggleButton />
                    <Box
                        ml={2}
                        display={{
                            base: 'inline-block',
                            md: 'none'
                        }}
                    >
                        <Menu>
                            <MenuButton
                                zIndex={0}
                                as={IconButton}
                                icon={<HamburgerIcon />}
                                variant="outline"
                                aria-label="Options"
                            />
                            <MenuList>
                                <MenuItem
                                    as={MenuLink}
                                    href="/"
                                >
                                    Home
                                </MenuItem>
                                <MenuItem
                                    href='/about-me'
                                    as={Link}
                                >
                                    About Me
                                </MenuItem>
                                <MenuItem
                                    as={Link}
                                    href="/journey"
                                >
                                    Journey
                                </MenuItem>
                                <MenuItem
                                    href="/software-projects"
                                    as={Link}
                                >
                                    Software Projects
                                </MenuItem>
                                <MenuItem
                                    as={Link}
                                    href={"/files/Craig_Celestin_Resume.pdf"}
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    Resume - Software Developer
                                </MenuItem>
                                <MenuItem
                                    href="/files/Craig_Celestin_AI_Resume.pdf"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    as={Link}
                                >
                                    AI Experience
                                </MenuItem>
                            </MenuList>
                        </Menu>
                    </Box>
                </Box>
            </Container >
        </Box >
    )
}

export default NavBar
