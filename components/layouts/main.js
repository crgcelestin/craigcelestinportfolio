import React from 'react'
import Head from "next/head";
import NavBar from "../navbar";
import { useRouter } from 'next/router'
import { Container, Box } from '@chakra-ui/react'

const Main = ({ children }) => {
    const router = useRouter()
    return (
        <>
            <Box as="main" pb={8}>
                <Head>
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <title>
                        Craig Celestin - Homepage
                    </title>
                </Head>
                <NavBar path={router.asPath} />
                <Container maxW="container.md" pt={14}>
                    {children}
                </Container>
            </Box>
        </>
    )
}

export default Main
