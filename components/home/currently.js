import {
    Box, Flex,
    List,
    ListItem, Text, UnorderedList
} from '@chakra-ui/react'
import ActionButton from '../actionbutton'
import NextLink from 'next/link'
import styled from '@emotion/styled'

const UrlSpan = styled.span`
cursor: pointer;
&:hover {
    text-decoration: underline;
    color: pink
}
`

export default function CurrentBio() {

    return (
        <>
            <UnorderedList>
                <ListItem style={{ marginTop: '20px', marginLeft: '10px' }}>
                    <Box textAlign="left" my={1}>
                        <Text>
                            <strong>{"Current Hack.Diversity Fellow - 2024 COHORT: NYC"}</strong>
                            {" - "}
                        </Text>
                        <List>
                            <ul>
                                <ListItem style={{ marginLeft: '30px' }}>
                                    Working on a FullStack x-ray assessment management platform and participating in a partnership pitch day with various software firms
                                </ListItem>
                            </ul>
                        </List>
                    </Box>
                </ListItem>
            </UnorderedList>
            <UnorderedList>
                <ListItem style={{ marginTop: '20px', marginLeft: '10px' }}>
                    <Box textAlign="left" my={1}>
                        <Text>
                            <strong>{"Currently developing and working with teams on various projects"}</strong>
                            {" - "}
                        </Text>
                        <List>
                            <ul>
                                <ListItem style={{ marginLeft: '30px' }}>
                                    Developing a prototype-centric &apos;for-engineer&apos; platform
                                </ListItem>
                            </ul>
                        </List>
                    </Box>
                </ListItem>
            </UnorderedList>
            <Flex direction="column">
                <Box mt={0} mb={3}></Box>
                <UnorderedList>
                    <ListItem style={{ marginTop: '20px', marginLeft: '10px' }}>
                        <Box style={{ marginTop: '10px' }} textAlign="left" my={1}>
                            <Text>
                                <strong>{"Backend Team Manager for an Event Management App @ NYC Code and Coffee"}</strong>
                                {
                                    " - Partnered with a team of 3 engineers to architect and develop a full-stack events management app with RESTful API methods"
                                }
                                <strong>
                                    {
                                        ' using SQlite, Svelte, Docker, OAuth2, FastApi '
                                    }
                                </strong>
                                <ul>
                                    <ListItem style={{ marginTop: '15px', marginLeft: '30px' }}>
                                        Partnered with a team of 3 engineers to architect and develop a full-stack events management app for creating/attending events, creating accounts/viewing account details, assigning users to groups and creating tasks lists
                                    </ListItem>
                                    <ListItem style={{ marginTop: '15px', marginLeft: '30px' }}>
                                        Optimized frontend design using Svelte to create an engaging and user-centric interface, while leveraging FastAPI for efficient backend development to enhance site performance, resulting in a seamless user experience
                                    </ListItem>
                                    <ListItem style={{ marginTop: '15px', marginLeft: '30px' }}>
                                        <Text>
                                            <strong>
                                                {'Currently working on implementing'}
                                            </strong>
                                            {
                                                " a seamless authentication and authorization solution, Event Management, Task Organization, and Notifications "
                                            }
                                        </Text>

                                    </ListItem>
                                    <Box>
                                        <NextLink
                                            target="_blank"
                                            rel="noopener noreferrer"
                                            href={'https://github.com/Ris345/eventexperienceapp'} passHref>
                                            <ActionButton style={{ marginTop: '10px' }} mt={0}>
                                                <Text fontWeight="bold" size="sm">
                                                    {"Event App ->"}
                                                </Text>
                                            </ActionButton>
                                        </NextLink>
                                    </Box>
                                </ul>
                            </Text>
                        </Box>
                    </ListItem>
                    <ListItem style={{ marginLeft: '10px' }}>
                        <Box style={{ marginTop: '10px' }} textAlign="left" my={1}>
                            <Text>
                                <strong>{"Recent Graduate of the AWS-Udacity AI Python Nanodegree Program"}</strong>
                                {
                                    " - Fulfilled Program Material: "
                                }
                            </Text>
                            <ul>
                                <ListItem style={{ marginLeft: '30px' }}>
                                    AWS Machine Learning Foundations
                                </ListItem>
                                <ListItem style={{ marginLeft: '30px' }}>
                                    Machine Learning Fundamentals:
                                    <ul>
                                        <ListItem style={{ marginLeft: '30px' }}>
                                            Intro to Machine Learning
                                            <ul>
                                                <NextLink
                                                    href='https://gitlab.com/crgcelestin/Bike-Sharing-Demand'
                                                    target="_blank"
                                                    rel="noopener noreferrer"
                                                    passHref
                                                >
                                                    <UrlSpan>
                                                        → Predict Bike Sharing Demand
                                                    </UrlSpan>
                                                </NextLink>
                                            </ul>
                                            <ListItem>
                                                Intro to Deep Learning
                                                <ul>
                                                    <NextLink
                                                        href='https://github.com/ccelest1/DigitsClassifier'
                                                        target="_blank"
                                                        rel="noopener noreferrer"
                                                        passHref
                                                    >
                                                        <UrlSpan>
                                                            → Digits Classifier
                                                        </UrlSpan>
                                                    </NextLink>
                                                </ul>
                                            </ListItem>
                                            <ListItem>
                                                Convolutional Neural Networks
                                                <ul>
                                                    <NextLink
                                                        href='https://github.com/ccelest1/LandmarkClassification'
                                                        target="_blank"
                                                        rel="noopener noreferrer"
                                                        passHref
                                                    >
                                                        <UrlSpan>
                                                            → Landmark Classification
                                                        </UrlSpan>
                                                    </NextLink>
                                                </ul>
                                            </ListItem>
                                        </ListItem>
                                    </ul>
                                </ListItem>
                            </ul>
                        </Box>
                    </ListItem>
                    <Box>
                        <NextLink
                            target="_blank"
                            rel="noopener noreferrer"
                            href={"/files/Craig_Celestin_AI_Resume.pdf"} passHref>
                            <ActionButton style={{ marginTop: '10px' }} mt={0}>
                                <Text fontWeight="bold" size="sm">
                                    {"AI Experience→"}
                                </Text>
                            </ActionButton>
                        </NextLink>
                    </Box>
                    <ListItem style={{ marginTop: '20px', marginLeft: '10px' }}>
                        <Box textAlign="left" my={1}>
                            <Text>
                                <strong>{"Currently learning about"}</strong>
                                {" - NoSQL, SQLite, NodeJs, Express, Svelte, and  Typescript"}
                            </Text>
                        </Box>
                    </ListItem>
                    <ListItem style={{ marginTop: '15px', marginLeft: '10px' }}>
                        <Box textAlign="left" my={1}>
                            <Text>
                                <strong>{"Currently improving my skills in"}</strong>
                                {" - React.js, JavaScript, Python, FastAPI, Data Structures and Algorithms"}
                            </Text>
                        </Box>
                    </ListItem>
                </UnorderedList>
            </Flex>
        </>
    )
}
