import { ExternalLinkIcon } from "@chakra-ui/icons";
import { Link, Text } from '@chakra-ui/react'
import NextLink from 'next/link'
import styled from "@emotion/styled";

const UrlSpan = styled.span`
cursor: pointer;
&:hover {
    text-decoration: underline;
    color: pink
}
`

export default function Bio() {
    return (
        <>
            <p style={{ marginTop: '10px' }}>
                Craig (Tyler) is an interdisciplinary Software Developer with experience in Full Stack Agile Development and AI Programming.
                Always a constant learner, he actively implores a growth mindset with the understanding that tech is a winding path of errors and troubleshooting. He values creating quality software through careful planning, design, and constant emphasis of user experience.
                His main motivation in software development is to drive impact and learn with others.
            </p>
            <p style={{ marginTop: '10px' }}>
                In his free time, he enjoys spending time with family, cooking and listening to a variety of podcasts and think pieces.  If you&apos;d like to chat, you can use the {' '}
                <Text as="u">
                    <NextLink href="#ContactForm">
                        <UrlSpan>
                            Contact Form
                        </UrlSpan>
                    </NextLink>
                </Text>
                {' '}
                or reach Craig at{' '}
                <Text as="u">
                    <NextLink
                        href="mailto: crgcelestin@gmail.com"
                        icon={<ExternalLinkIcon mx='2px' />}
                    >
                        <Link>
                            crgcelestin@gmail.com
                        </Link>
                    </NextLink>
                </Text>
            </p>
        </>
    )
}
