import {
    Box, Text,
    Flex, UnorderedList, ListItem
} from '@chakra-ui/react'
import NextLink from 'next/link'
import ActionButton from '../actionbutton'

const PastDoings = () => {
    return (
        <>
            <Flex direction="column">
                <Box mt={0} mb={3}></Box>
                <UnorderedList>
                    <ListItem style={{ marginLeft: '10px' }}>
                        <Box style={{ marginTop: '10px' }} textAlign="left" my={1}>
                            <Text>
                                <strong>{"Software Engineer"}</strong>
                                {
                                    " at "
                                }
                                <NextLink
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href={'https://atlanticautomation.ai'} passHref>
                                    <strong style={{ textDecoration: 'underline' }}>{"Atlantic Automation"}</strong>
                                </NextLink>
                            </Text>
                            <ul>
                                <ListItem style={{ marginTop: '10px', marginLeft: '30px' }}>
                                    Designed and tested Python scripts using Pandas and Matplotlib that automated financial report creation, led to a reduction in time spent on report generation by 90%
                                </ListItem>
                                <ListItem style={{ marginTop: '10px', marginLeft: '30px' }}>
                                    Refactored automation script to have component based data visualization via class and object instances
                                </ListItem>
                                <ListItem style={{ marginTop: '10px', marginLeft: '30px' }}>
                                    Developed an analytics dashboard using PowerBi within a 2-week sprint, streamlining data analysis and easing data-driven decision-making processes by 95%
                                </ListItem>
                            </ul>
                        </Box>
                    </ListItem>
                    <Box>
                        <NextLink
                            target="_blank"
                            rel="noopener noreferrer"
                            href={'https://gitlab.com/crgcelestin/atlanticautomation'} passHref>
                            <ActionButton style={{ marginTop: '10px' }} mt={0}>
                                <Text fontWeight="bold" size="sm">
                                    {"Atlantic Automation Repo ->"}
                                </Text>
                            </ActionButton>
                        </NextLink>
                    </Box>
                </UnorderedList>
                <UnorderedList>
                    <ListItem style={{ marginLeft: '10px' }}>
                        <Box style={{ marginTop: '10px' }} textAlign="left" my={1}>
                            <Text>
                                <strong>{"Fullstack Software Engineering Graduate"}</strong>
                                {
                                    " from "
                                }
                                <NextLink
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href={'https://www.galvanize.com/hack-reactor/beginner/full-time-bootcamp/'} passHref>
                                    <strong style={{ textDecoration: 'underline' }}>{"Hack Reactor"}</strong>
                                </NextLink>
                            </Text>
                            <ul>
                                <ListItem style={{ marginTop: '10px', marginLeft: '30px' }}>
                                    Participated in Full Stack Architecture Development involving turning Software Requirements into a running application with Python and Django, Vanilla and JavaScript ES6
                                </ListItem>
                                <ListItem style={{ marginTop: '10px', marginLeft: '30px' }}>
                                    Wrote HTML and CSS to build working and interactive Web apps
                                </ListItem>
                                <ListItem style={{ marginTop: '10px', marginLeft: '30px' }}>
                                    Used a relational PostgreSQL database to store and retrieve data.
                                </ListItem>
                                <ListItem style={{ marginTop: '10px', marginLeft: '30px' }}>
                                    Used a number of protocols and formats to interact w/ data in messaging middleware.
                                </ListItem>
                                <ListItem style={{ marginTop: '10px', marginLeft: '30px' }}>
                                    Applied the latest design and technological principles to create Microservices.
                                </ListItem>
                                <ListItem style={{ marginTop: '10px', marginLeft: '30px' }}>
                                    Created stand-alone web app frontends w/ functional react, hooks, redux. Built real-time apps that update their UIs w/ WebSockets and FastAPI.
                                </ListItem>
                                <ListItem style={{ marginTop: '10px', marginLeft: '30px' }}>
                                    Used various persistent data stores for different data types with MongoDB, Apache Kafka. Planned, created, and monitored CI and Pipeline Delivery with GitLab Pipelines
                                </ListItem>
                            </ul>
                        </Box>
                    </ListItem>
                </UnorderedList>
                <UnorderedList>
                    <ListItem style={{ marginLeft: '10px' }}>
                        <Box style={{ marginTop: '10px' }} textAlign="left" my={1}>
                            <Text>
                                <strong>{"Energy Analyst"}</strong>
                                {
                                    " at "
                                }
                                <NextLink
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href={'https://www.coienergy.com'} passHref>
                                    <strong style={{ textDecoration: 'underline' }}>{"COI Energy"}</strong>
                                </NextLink>
                            </Text>
                            <ul>
                                <ListItem style={{ marginTop: '10px', marginLeft: '30px' }}>
                                    Researched and reported on critical facets of the energy industry as well as changing regulations and field advancements
                                </ListItem>
                                <ListItem style={{ marginTop: '10px', marginLeft: '30px' }}>
                                    Participated in the Quality Analysis Process for web and mobile platforms regarding Demand Response for Business, Utility, and Admin roles.
                                </ListItem>
                                <ListItem style={{ marginTop: '10px', marginLeft: '30px' }}>
                                    Monitored and analyzed daily operations&apos; data to identify trends and process improvement opportunities. Developed project timelines and managed critical milestones to ensure on-time and under-budget delivery
                                </ListItem>
                                <ListItem style={{ marginTop: '10px', marginLeft: '30px' }}>
                                    Worked with Energy Data and Demand Response modeling using Jupyter Notebook
                                </ListItem>
                            </ul>
                        </Box>
                    </ListItem>
                </UnorderedList>
            </Flex>
        </>
    )
}

export default PastDoings;
