import { useForm } from 'react-hook-form'
import {
  FormErrorMessage,
  FormLabel,
  FormControl,
  Input,
  Button,
  useColorModeValue,
  Textarea
} from '@chakra-ui/react'


export default function HookForm() {
  const {
    handleSubmit,
    register,
    reset,
    formState: { errors, isSubmitting },
  } = useForm()


  async function onSubmit(values) {
    try {
      const formData = new FormData();
      Object.entries(values).forEach(([key, value]) => {
        formData.append(key, value);
      });
      const response = await fetch("https://getform.io/f/4868ac68-55de-4101-b760-cb8a764d9fbd", {
        method: "POST",
        body: formData
      });
      if (response.ok) {
        // Handle successful form submission
        alert("Form submitted successfully");
        // Reset form fields
        reset();
      } else {
        // Handle form submission failure
        alert("Form submission failed");
      }
    } catch (error) {
      // Handle any errors that occur during form submission
      console.error("Error submitting form:", error);
      alert("An error occurred while submitting the form");
    }
  };
  return (
    <div style={{ position: 'relative', minHeight: '50vh', paddingBottom: '100px' }}>
      <div style={{ position: 'absolute', bottom: 0, left: 0, right: 0 }}>
        <h1 style={{ fontWeight: 'bold', fontSize: '24px', marginBottom: '16px' }}>Contact Form</h1>
        <form onSubmit={handleSubmit(onSubmit)}
          style={{ border: '1px solid #ccc', padding: '16px', borderRadius: '8px', backgroundColor: 'glassTeal' }}>
          <FormControl isInvalid={errors.name}>
            <FormLabel htmlFor='enterinfo'>Enter Your Information Here!</FormLabel>
            <Input
              id='name'
              _placeholder={useColorModeValue('gray.500', 'black')}
              placeholder='Name'
              {
              ...register('name', {
                required: 'This is required',
                minLength: { value: 4, message: 'Minimum length should be 4' },
              })
              }
              style={{ marginBottom: '8px', padding: '8px', borderRadius: '4px', border: '1px solid #ccc' }}
            />
            <Input
              id='email'
              _placeholder={useColorModeValue('gray.500', 'black')}
              placeholder='Email'
              {...register('email', {
                required: 'This is required',
                minLength: { value: 4, message: 'Minimum length should be 4' },
              })}
              style={{ marginBottom: '8px', padding: '8px', borderRadius: '4px', border: '1px solid #ccc' }}
            />
            <Textarea
              id='message'
              _placeholder={useColorModeValue('gray.500', 'black')}
              placeholder='Message'
              {...register('email', {
                required: 'This is required',
                minLength: { value: 4, message: 'Minimum length should be 4' },
              })}
              style={{ marginBottom: '8px', padding: '8px', borderRadius: '4px', border: '1px solid #ccc' }}
            />
            <FormErrorMessage>
              {errors.name && errors.name.message}
            </FormErrorMessage>
          </FormControl>
          <Button mt={4} colorScheme='teal' isLoading={isSubmitting} type='submit' style={{ padding: '8px 16px', borderRadius: '4px', backgroundColor: '#319795', color: '#fff', fontWeight: 'bold', border: 'none' }}>
            Submit
          </Button>
        </form>
      </div>
    </div >
  )
}
