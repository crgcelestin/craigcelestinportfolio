import {
    ListItem,
    Flex,
    UnorderedList,
    Box,
    ListIcon,
    Heading,
    Text
} from '@chakra-ui/react'
import NextLink from 'next/link'
import { IoLogoPython, IoLogoHtml5, IoLogoCss3, IoLogoReact, IoLogoJavascript, IoLogoNodejs, IoLogoGitlab } from "react-icons/io5";
import { SiDjango, SiFastapi, SiRabbitmq, SiPostgresql, SiMongodb, SiDocker, SiRsocket, SiSqlite, SiPytest, SiTestinglibrary, SiInsomnia } from "react-icons/si";
import ActionButton from '../actionbutton';


export default function AboutMeEducation() {

    return (
        <Flex direction="column">
            <Box my={4}>
                <Heading as='h3' variant='section-title' fontSize="3xl" fontWeight="bold">
                    {"Education"}
                </Heading>
            </Box>
            <Box mt={3}>
                <Text fontSize="1.1rem" fontWeight="bold">
                    <Heading variant='section-title'
                        textDecoration="underline"
                        style={{ display: "inline" }}
                    >
                        {"AWS AI & ML Scholarship Program"}
                    </Heading>
                    {" - "}
                    <Text
                        fontSize="1.1rem"
                        fontWeight="normal"
                        textDecoration="none"
                        style={{ display: "inline" }}
                    >
                        {"AI Scholar"}
                    </Text>
                </Text>
                <UnorderedList spacing={1} ml={6}>
                    <li>
                        <ListItem style={{ marginTop: '20px' }}>
                            140 hours of AI Python Programming Curriculum culminating in two projects:
                            <ul>
                                <ListItem style={{ marginTop: '10px', marginLeft: '20px' }}>
                                    Implementing an algorithmic framework to ID Dog Breeds using a pre trained image classifier (vgg16, densenet, arch)
                                </ListItem>
                                <ListItem style={{ marginTop: '10px', marginLeft: '20px' }}>
                                    Creating my own image classifier that was able to identify various species of flowers from user provided images - involved training neural networks and using pytorch to design deep learning models
                                </ListItem>
                            </ul>
                        </ListItem>
                    </li>
                </UnorderedList>
            </Box>
            <Box>
                <NextLink
                    target="_blank"
                    rel="noopener noreferrer"
                    href={"/files/Craig_Celestin_AI_Resume.pdf"} passHref>
                    <ActionButton style={{ marginTop: '10px' }} mt={0}>
                        <Text fontWeight="bold" size="sm">
                            {"AI Experience→"}
                        </Text>
                    </ActionButton>
                </NextLink>
            </Box>
            <Box mt={0}>
                <Heading variant='section-title' fontWeight="bold" textDecoration="underline">
                    {"Hack Reactor"}
                </Heading>
                <UnorderedList spacing={1} ml={6}>
                    <ListItem style={{ marginTop: '20px', wordBreak: 'keep-all', whiteSpace: 'normal' }}>
                        {/*
                        In this version, we're using wordBreak: 'keep-all' to ensure that the word "full-stack" is kept together without breaking. The whiteSpace: 'normal' still allows the text to wrap as needed.
                         */}
                        19-week full-time Software Engineering program focusing on building full-stack web applications
                    </ListItem>
                    <ListItem>
                        Spent over 1000 hours of coding, deep diving into:
                        <UnorderedList spacing={1} ml={3}>
                            <ListItem>
                                <Box as="span" fontWeight="bold">Programming Languages:</Box>
                                <ListIcon as={IoLogoJavascript} color="yellow.400" ml={1} mr={0.5} mb={0.5} />
                                JavaScript ES6,
                                <ListIcon as={IoLogoPython} color="blue.500" ml={1} mr={0.5} mb={0.5} />
                                Python 3
                            </ListItem>
                            <ListItem>
                                <Box as="span" fontWeight="bold">Front-End:</Box>
                                <ListIcon as={IoLogoReact} color="cyan.300" ml={1} mr={0.5} mb={0.5} />
                                React,
                                <ListIcon as={IoLogoHtml5} color="red.500" ml={1} mr={0.5} mb={0.5} />
                                HTML5,
                                <ListIcon as={IoLogoCss3} color="blue.600" ml={1} mr={0.5} mb={0.5} />
                                CSS, DOM Manipulation
                            </ListItem>
                            <ListItem>
                                <Box as="span" fontWeight="bold">Back-End:</Box>
                                <ListIcon as={SiDjango} color="green.800" ml={1} mr={0.5} mb={0} />
                                Django 4,
                                <ListIcon as={SiFastapi} color="teal.500" ml={1} mr={0.5} mb={0.5} />
                                FastAPI,
                                <ListIcon as={SiRabbitmq} color="#f66300" ml={1} mr={1} mb={0.5} />
                                RabbitMQ,
                                <ListIcon as={SiPostgresql} color="blue.600" ml={1} mr={1} mb={0} />
                                PostgreSQL,
                                <ListIcon as={SiMongodb} color="green.500" ml={1} mr={0} mb={0.5} />
                                MongoDB,
                                <ListIcon as={IoLogoNodejs} color="green.500" ml={1} mr={0} mb={0.5} />
                                NodeJs,
                                <ListIcon as={SiRsocket} color="green.500" ml={1} mr={0} mb={0.5} />
                                WebSockets,
                                <ListIcon as={SiSqlite} color="green.500" ml={1} mr={0} mb={0.5} />
                                SQLite
                            </ListItem>
                            <ListItem>
                                <Box as="span" fontWeight="bold">System Design:</Box>
                                <ListIcon as={SiDocker} color="blue.300" ml={1} mr={0.5} mb={0.5} />
                                Docker, Monoliths, Microservices, Domain-Driven Design, Message Queues, Polling
                            </ListItem>
                            <ListItem>
                                <Box as="span" fontWeight="bold">Testing:</Box>
                                <ListIcon as={SiPytest} color="blue.300" ml={1} mr={0.5} mb={0.5} />
                                PyTest,
                                <ListIcon as={SiTestinglibrary} color="green.300" ml={1} mr={0.5} mb={0.5} />
                                TestClient
                            </ListItem>
                            <ListItem>
                                <Box as="span" fontWeight="bold">Development:</Box>
                                <ListIcon as={IoLogoGitlab} color="blue.300" ml={1} mr={0.5} mb={0.5} />
                                Gitlab,
                                <ListIcon as={SiInsomnia} color="blue.300" ml={1} mr={0.5} mb={0.5} />
                                Insomnia, pgAdmin, Agile, 3rd Party APIs
                            </ListItem>
                        </UnorderedList>
                    </ListItem>
                </UnorderedList>
            </Box>
            <Box mt={3}>
                <Text fontSize="1.1rem" fontWeight="bold">
                    <Heading variant='section-title'
                        textDecoration="underline"
                        style={{ display: "inline" }}
                    >
                        {"Binghamton University"}
                    </Heading>
                    {" - "}
                    <Text
                        fontSize="1.1rem"
                        fontWeight="normal"
                        textDecoration="none"
                        style={{ display: "inline" }}
                    >
                        {"Chemistry with Engineering minor"}
                    </Text>
                </Text>
                <UnorderedList spacing={1} ml={6}>
                    <li>
                        <ListItem style={{ marginTop: '20px' }}>
                            Activities and societies:
                            <ul>
                                <ListItem style={{ marginLeft: '20px' }}>
                                    Treasurer Transfer Student Association
                                </ListItem>
                                <ListItem style={{ marginLeft: '20px' }}>
                                    SES (Smart Energy Scholars) Member
                                </ListItem>
                                <ListItem style={{ marginLeft: '20px' }}>
                                    Work Study
                                </ListItem>
                                <ListItem style={{ marginLeft: '20px' }}>
                                    McNair Graduate Program
                                </ListItem>
                                <ListItem style={{ marginLeft: '20px' }}>
                                    NSBE (National Society for Black Engineers)
                                </ListItem>
                            </ul>
                        </ListItem>
                    </li>
                </UnorderedList>
            </Box>
            <Box mt={3}>
                <Text fontSize="1.1rem" fontWeight="bold">
                    <Heading variant='section-title'
                        textDecoration="underline"
                        style={{ display: "inline" }}
                    >
                        {"Data Science For All"}
                    </Heading>
                    {" - "}
                    <Text
                        fontSize="1.1rem"
                        fontWeight="normal"
                        textDecoration="none"
                        style={{ display: "inline" }}
                    >
                        {"DS4A Fellow"}
                    </Text>
                </Text>
                <UnorderedList spacing={1} ml={6}>
                    <ListItem style={{ marginTop: '20px' }}>
                        Completed a successful mentorship project with partnering bank Creditas (Brazilian Financial Firm) enabling a UI and dashboard providing customer analytics
                    </ListItem>
                </UnorderedList>
            </Box>
            <Box my={4}>
                <Heading as='h3' variant='section-title' fontSize="3xl" fontWeight="bold">
                    {"Certifications"}
                </Heading>
            </Box>
            <Box mt={0}>
                <Box display="block">
                    <NextLink
                        href={"files/AI_Programming_Nanodegree.pdf"}
                        target="_blank"
                        rel="noopener noreferrer"
                        passHref
                    >
                        <Text
                            fontSize="1rem"
                            textDecoration="underline"
                            className="hoverable"
                            as="a"

                        >
                            {"AWS-Udacity Python Nanodegree"}
                        </Text>
                    </NextLink>
                </Box>
                <Box display="block">
                    <NextLink
                        href={"files/DSA_Masterclass.pdf"}
                        target="_blank"
                        rel="noopener noreferrer"
                        passHref
                    >
                        <Text
                            fontSize="1rem"
                            textDecoration="underline"
                            className="hoverable"
                            as="a"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            {"Data Structures and Algorithms Masterclass"}
                        </Text>
                    </NextLink>
                </Box>
                <Box display="block">
                    <NextLink
                        href={"/files/Hack_Reactor_Craig_Celestin.pdf"}
                        target="_blank"
                        rel="noopener noreferrer"
                        passHref
                    >
                        <Text
                            fontSize="1rem"
                            textDecoration="underline"
                            className="hoverable"
                            as="a"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            {"Hack Reactor Certificate of Completion"}
                        </Text>
                    </NextLink>
                </Box>
            </Box>

        </Flex>
    );
}
