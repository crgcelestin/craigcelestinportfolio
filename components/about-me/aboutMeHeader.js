import {
    Stack,
    Flex,
    Heading,
    Box,
    Image,
    Spacer,
    useColorModeValue,
    useMediaQuery
} from '@chakra-ui/react'
import { useState, useEffect } from 'react';
import AboutMeContact from './aboutMeContact'


export default function AboutMeHeader() {
    const [desktopQuery] = useMediaQuery('(min-width: 700px');
    const [isMinWidth, setIsMinWidth] = useState(false);

    useEffect(() => {
        if (desktopQuery !== isMinWidth) {
            setIsMinWidth(desktopQuery);
        }
    }, [isMinWidth, desktopQuery]);
    return (
        <Stack >
            <Flex direction={isMinWidth ? "row" : "column"} mt={isMinWidth ? 15 : 0}>
                <Image

                    src='/images/AboutMe.jpeg'
                    alt='profile image'
                    maxWidth={isMinWidth ? '200px' : '80%'}
                    maxHeight={isMinWidth ? '200px' : '80%'}
                    display={isMinWidth ? "none" : "block"}
                    bgGradient='linear(to-r, green.200, pink.500)'
                    style={{ marginRight: '1em', marginTop: '100px' }}
                >
                </Image>
                <Image
                    borderColor={useColorModeValue('gray.800', 'whiteAlpha.900')}
                    borderWidth={2}
                    borderStyle='solid'
                    display={isMinWidth ? "block" : "none"}
                    maxHeight={isMinWidth ? '250px' : '80%'}
                    src='/images/AboutMe.jpeg'
                    alt='profile image'
                    style={{ marginRight: '1em', marginTop: '60px' }}
                    bgGradient='linear(to-r, green.200, pink.500)'
                />
                <Spacer></Spacer>
                <Box
                    ml={isMinWidth ? 2 : 0}
                    my={isMinWidth ? 20 : 3}
                    marginTop='20px'>
                    <Heading
                        fontSize="4xl"
                        align={isMinWidth ? "right" : "left"}
                        mt={isMinWidth ? -5 : 0}
                        variant='section-title'>
                        {" About Me "}
                    </Heading>
                    <AboutMeContact />
                </Box>
            </Flex>
        </Stack>
    )
}
