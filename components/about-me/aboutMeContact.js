import {
    Button,
    Flex,
    SimpleGrid,
    Text,
    useColorModeValue,
    useMediaQuery
} from '@chakra-ui/react'
import { useState, useEffect } from 'react'
import NextLink from 'next/link'
import {
    FaGithub,
    FaLinkedinIn,
    FaGitlab
} from 'react-icons/fa'
import {
    MdAlternateEmail,
    MdOutlinePictureAsPdf
} from 'react-icons/md'


export default function AboutMeContact() {
    const [desktopQuery] = useMediaQuery('(min-width: 700px');
    const [isMinWidth, setIsMinWidth] = useState(false)

    useEffect(() => {
        if (desktopQuery !== isMinWidth) {
            setIsMinWidth(desktopQuery)
        }
    }, [isMinWidth, desktopQuery])
    return (
        <Flex direction={["block", "block", "block", "block"]}>
            <SimpleGrid columns={[1, 1, 2, 2]} spacing={10} mt={10}>
                <NextLink href="mailto: crgcelestin@gmail.com" passHref>
                    <Button
                        as="a"
                        target="_blank"
                        size="md"
                        leftIcon={<MdAlternateEmail />}
                        borderRadius="sm"
                        borderWidth="1px"
                        colorScheme="grey"
                        variant="outline"
                        fontWeight="bold"
                        border={useColorModeValue('gray.800', 'whiteAlpha.900')}
                        _hover={{
                            background: "red.100",
                            color: useColorModeValue('white', 'black'),
                            border: "1px solid",
                            borderColor: "transparent",
                        }}
                    >
                        <Text fontSize="sm" fontWeight="bold">
                            {"Send an Email"}
                        </Text>
                    </Button>
                </NextLink>

                <NextLink href="https://www.linkedin.com/in/craig-celestin/"
                    target="_blank"
                    rel="noopener noreferrer"
                    passHref>
                    <Button
                        as="a"
                        target="_blank"
                        size="md"
                        leftIcon={<FaLinkedinIn />}
                        borderRadius="sm"
                        borderWidth="1px"
                        colorScheme="grey"
                        variant="outline"
                        fontWeight="bold"
                        border={useColorModeValue('gray.800', 'whiteAlpha.900')}
                        _hover={{
                            background: "blue.100",
                            color: useColorModeValue('white', 'black'),
                            border: "1px solid",
                            borderColor: "transparent",
                        }}
                    >
                        <Text fontSize="sm" fontWeight="bold">
                            {"View LinkedIn"}
                        </Text>
                    </Button>
                </NextLink>
                <NextLink href="https://gitlab.com/crgcelestin"
                    target="_blank"
                    rel="noopener noreferrer"
                    passHref>
                    <Button
                        as="a"
                        target="_blank"
                        size="md"
                        leftIcon={<FaGitlab />}
                        borderRadius="sm"
                        borderWidth="1px"
                        colorScheme="grey"
                        variant="outline"
                        fontWeight="bold"
                        border={useColorModeValue('gray.800', 'whiteAlpha.900')}
                        _hover={{
                            background: "green.100",
                            color: useColorModeValue('white', 'black'),
                            border: "1px solid",
                            borderColor: "transparent",
                        }}
                    >
                        <Text fontSize="sm" fontWeight="bold">
                            {"View GitLab"}
                        </Text>
                    </Button>
                </NextLink>

                <NextLink
                    href="https://github.com/ccelest1"
                    target="_blank"
                    rel="noopener noreferrer"
                    passHref>
                    <Button
                        as="a"
                        target="_blank"
                        size="md"
                        leftIcon={<FaGithub />}
                        borderRadius="sm"
                        borderWidth="1px"
                        colorScheme="grey"
                        variant="outline"
                        fontWeight="bold"
                        border={useColorModeValue('gray.800', 'whiteAlpha.900')}
                        _hover={{
                            background: "yellow.100",
                            color: useColorModeValue('white', 'black'),
                            border: "1px solid",
                            borderColor: "transparent",
                        }}
                    >
                        <Text fontSize="sm" fontWeight="bold">
                            {"View GitHub"}
                        </Text>
                    </Button>
                </NextLink>

                <NextLink
                    href={"/files/Craig_Celestin_Resume.pdf"}
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    <Button
                        as="a"
                        target="_blank"
                        size="md"
                        leftIcon={<MdOutlinePictureAsPdf />}
                        borderRadius="sm"
                        borderWidth="1px"
                        colorScheme="grey"
                        variant="outline"
                        fontWeight="bold"
                        border={useColorModeValue('gray.800', 'whiteAlpha.900')}
                        _hover={{
                            background: "purple.100",
                            color: useColorModeValue('white', 'black'),
                            border: "1px solid",
                            borderColor: "transparent",
                        }}
                    >
                        <Text fontSize="sm" fontWeight="bold">
                            {"View resume"}
                        </Text>
                    </Button>
                </NextLink>
                <NextLink target="_blank"
                    rel="noopener noreferrer"
                    href="/#ContactForm" passHref>
                    <Button
                        as="a"
                        size="md"
                        leftIcon={<MdAlternateEmail />}
                        borderRadius="sm"
                        borderWidth="1px"
                        colorScheme="grey"
                        variant="outline"
                        fontWeight="bold"
                        border={useColorModeValue('gray.800', 'whiteAlpha.900')}
                        _hover={{
                            background: "red.100",
                            color: useColorModeValue('white', 'black'),
                            border: "1px solid",
                            borderColor: "transparent",
                        }}
                    >
                        <Text fontSize="sm" fontWeight="bold">
                            {"Contact Form"}
                        </Text>
                    </Button>
                </NextLink>
            </SimpleGrid>
        </Flex>
    )
}
