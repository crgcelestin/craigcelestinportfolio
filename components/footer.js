import { Box, Image, useColorModeValue, Text, Flex, Center } from '@chakra-ui/react'

const Footer = () => {
    const SoftwareImg = `/images/logo${useColorModeValue('', '-dark')}.png`
    return (
        <Center mt={4} mb={3}>
            <Box position='relative' padding='10'>
                <Flex alignItems="center">
                    <Text as="span" display="inline-block" width="20px" textAlign="center">
                        --
                    </Text>
                    <Image
                        src={SoftwareImg}
                        width={10}
                        height={10}
                        alt='logo'

                    />
                    <Text as="span" display="inline-block" width="20px" textAlign="center" >
                        --
                    </Text>
                </Flex>
            </Box>
        </Center>
    )
}

export default Footer;
