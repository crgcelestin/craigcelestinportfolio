import {
    Flex,
    Stack,
    ListIcon,
    ListItem,
    UnorderedList,
    useColorModeValue,
} from '@chakra-ui/react'
import ProjectCardItem from "./project-card-template";
import { ListContainer } from './levelContainer';
import { IoLogoJavascript, IoLogoPython, IoLogoReact } from "react-icons/io5";
import { SiDjango, SiRabbitmq, SiPostgresql, SiDocker, SiBootstrap } from "react-icons/si";
import Footer from "../footer";

export default function PastProjectsList() {
    const Wardrobify = `/images/closetio${useColorModeValue('', '-dark')}.png`
    const CarShop = `/images/carshop${useColorModeValue('', '-dark')}.png`
    return (
        <Flex>
            <Stack
                spacing={5}
                align='center'
            >
                <ProjectCardItem
                    title='Car Shop'
                    desc={
                        'React web app for dealership management with servicing, sales, and inventory as separate microservices enabling efficient autoshop ownership'
                    }
                    contributions={
                        <>
                            <UnorderedList spacing={1} ml={6}>
                                <ul>
                                    <ListItem style={{ marginTop: '5px' }} >
                                        Built React component library with state management and modern fetching to direct data via API calls
                                    </ListItem>
                                    <ListItem style={{ marginTop: '5px' }}>
                                        Developed a dockerized RESTful API infrastructure to manage throughput for a dealership at scale
                                    </ListItem>
                                    <ListItem style={{ marginTop: '5px' }}>
                                        Employed conditional rendering in order to enable the user to accept and revoke appointments, and view appointments accompanied by a VIP status
                                    </ListItem>
                                    <ListItem style={{ marginTop: '5px' }}>
                                        Executed filtered search of the auto shop, allowing for querying of vehicles through VIN, Car Type, and Name
                                    </ListItem>
                                    <ListItem style={{ marginTop: '5px' }}>
                                        Refactored the Sales side to allow for searching of sales records by sales person using an inputChange and onClick Handler
                                    </ListItem>
                                    <ListItem>
                                        Implemented a GET Request to filter sales records via a specified SalesPerson instance
                                    </ListItem>
                                </ul>
                            </UnorderedList>
                        </>
                    }
                    icon={CarShop}
                    tags={[
                        <ListContainer key={1}>
                            <ListItem>
                                <ListIcon as={SiDjango} color="green.500" ml={0} mr={1} mb={0.5} />
                                Django
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={2}>
                            <ListItem>
                                <ListIcon as={IoLogoPython} color="blue.500" ml={0} mr={1} mb={0.5} />
                                Python 3
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={3}>
                            <ListItem>
                                <ListIcon as={IoLogoReact} color="cyan.300" ml={0} mr={1} mb={0.5} />
                                React.js
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={4}>
                            <ListItem>
                                <ListIcon as={IoLogoJavascript} color="yellow.400" ml={0} mr={1} mb={0.5} />
                                JavaScript ES6
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={5}>
                            <ListItem >
                                <ListIcon as={SiBootstrap} color="purple.600" ml={0} mr={1} mb={0.5} />
                                Bootstrap
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={6}>
                            <ListItem>
                                <ListIcon as={SiPostgresql} color="blue.600" ml={0} mr={1} mb={0} />
                                PostgreSQL
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={7}>
                            <ListItem>
                                <ListIcon as={SiDocker} color="blue.600" ml={0} mr={1} mb={0} />
                                Docker
                            </ListItem>
                        </ListContainer>
                    ]}
                    gh_link={"https://gitlab.com/crgcelestin/car-shop"}
                    video="https://www.youtube.com/embed/9kyNphT3cEU"
                >
                </ProjectCardItem >
                <Footer></Footer>
                <ProjectCardItem
                    title='Wardrobify'
                    desc={
                        'Built a virtual clothing storage solution to aid users in keeping track of the inventory of shoes and hats. Users can utilize locations for hats and bins for shoes in order to create closet storage distinctions'

                    }
                    contributions={
                        <>
                            <UnorderedList spacing={1} ml={6}>
                                <ul>
                                    <ListItem style={{ marginTop: '5px' }} >
                                        Built React component library with state management and modern fetching to direct data via API calls
                                    </ListItem>
                                    <ListItem style={{ marginTop: '5px' }}>
                                        Developed a dockerized RESTful API infrastructure to manage user&apos;s inventory
                                    </ListItem>
                                    <ListItem style={{ marginTop: '5px' }}>
                                        Implemented Rabbitmq allowing for the polling of locations and bins for item allocation
                                    </ListItem>
                                </ul>
                            </UnorderedList>
                        </>
                    }
                    icon={Wardrobify}
                    tags={[
                        <ListContainer key={8}>
                            <ListItem>
                                <ListIcon as={SiDjango} color="green.500" ml={0} mr={1} mb={0.5} />
                                Django
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={9}>
                            <ListItem>
                                <ListIcon as={IoLogoPython} color="blue.500" ml={0} mr={1} mb={0.5} />
                                Python 3
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={10}>
                            <ListItem>
                                <ListIcon as={IoLogoReact} color="cyan.300" ml={0} mr={1} mb={0.5} />
                                React.js
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={11}>
                            <ListItem>
                                <ListIcon as={IoLogoJavascript} color="yellow.400" ml={0} mr={1} mb={0.5} />
                                JavaScript ES6
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={12}>
                            <ListItem >
                                <ListIcon as={SiBootstrap} color="purple.600" ml={0} mr={1} mb={0.5} />
                                Bootstrap
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={13}>
                            <ListItem>
                                <ListIcon as={SiPostgresql} color="blue.600" ml={0} mr={1} mb={0} />
                                PostgreSQL
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={14}>
                            <ListItem>
                                <ListIcon as={SiDocker} color="blue.600" ml={0} mr={1} mb={0} />
                                Docker
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={15}>
                            <ListItem>
                                <ListIcon as={SiRabbitmq} color="red.600" ml={0} mr={1} mb={0} />
                                Rabbitmq
                            </ListItem>
                        </ListContainer>
                    ]}
                    gh_link={"https://gitlab.com/hr-mod-2-retake-crgcelestin/microservice-two-shot"}
                    video="https://www.youtube.com/embed/DoA8Uv4_fGQ"
                >
                </ProjectCardItem >
                <Footer></Footer>
            </Stack>
        </Flex>
    )
}
