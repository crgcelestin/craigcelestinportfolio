import {
    Flex,
    Stack,
    ListIcon,
    ListItem,
    UnorderedList,
    useColorModeValue
} from '@chakra-ui/react'
import ProjectCardItem from "./project-card-template";
import { IoLogoJavascript, IoLogoPython, IoLogoReact, IoInfinite } from "react-icons/io5";
import { SiFastapi, SiPostgresql, SiDocker, SiBootstrap } from "react-icons/si";
import { ListContainer } from './levelContainer';
import { TiDeviceDesktop } from 'react-icons/ti';

export default function CurrentProjectsList() {
    const FitsterIO = `/images/fisteriov2${useColorModeValue('', '-dark')}.png`

    return (
        <Flex>
            <Stack
                spacing={5}
                align='center'
            >
                <ProjectCardItem
                    title='Fitster.io'
                    desc={
                        'Community-centric wardrobe virtualization experience with search capabilities'
                    }
                    contributions={
                        <>
                            <UnorderedList spacing={1} ml={6}>
                                <ul>
                                    <ListItem style={{ marginTop: '5px' }}>
                                        Successfully performed database setup using PostgreSQL and Docker to enable BackEnd Operations with Fastapi
                                    </ListItem>
                                    <ListItem style={{ marginTop: '5px' }} >
                                        Aided in query, router implementation for Users, Outfits, and Posts in order to implement functional endpoints
                                    </ListItem>
                                    <ListItem>
                                        Provided infrastructure for search of user profiles, user details, outfit search, and post details with the implementation of Search Parameters
                                    </ListItem>
                                    <ListItem style={{ marginTop: '5px' }}>
                                        Executed on user login setup and authorization using JWTs and AuthProvider (User token and session info)
                                    </ListItem>
                                    <ListItem style={{ marginTop: '5px' }}>
                                        Architected backend unit tests to confirm successful user creation, posting, and user-search using TestClient
                                    </ListItem>
                                </ul>
                            </UnorderedList>
                        </>
                    }
                    icon={FitsterIO}
                    tags={[
                        <ListContainer key={24}>
                            <ListItem>
                                <ListIcon as={SiFastapi} color="teal.500" ml={0} mr={1} mb={0.5} />
                                FastAPI
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={25}>
                            <ListItem>
                                <ListIcon as={IoLogoPython} color="blue.500" ml={0} mr={1} mb={0.5} />
                                Python 3
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={26}>
                            <ListItem>
                                <ListIcon as={IoLogoReact} color="cyan.300" ml={0} mr={1} mb={0.5} />
                                React.js
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={27}>
                            <ListItem>
                                <ListIcon as={IoLogoJavascript} color="yellow.400" ml={0} mr={1} mb={0.5} />
                                JavaScript ES6
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={28}>
                            <ListItem >
                                <ListIcon as={SiBootstrap} color="purple.600" ml={0} mr={1} mb={0.5} />
                                Bootstrap
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={29}>
                            <ListItem>
                                <ListIcon as={SiPostgresql} color="blue.600" ml={0} mr={1} mb={0} />
                                PostgreSQL
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={30}>
                            <ListItem>
                                <ListIcon as={SiDocker} color="blue.600" ml={0} mr={1} mb={0} />
                                Docker
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={31}>
                            <ListItem>
                                <ListIcon as={IoInfinite} color="blue.600" ml={0} mr={1} mb={0} />
                                CI/CD
                            </ListItem>
                        </ListContainer>,
                        <ListContainer key={32}>
                            <ListItem>
                                <ListIcon as={TiDeviceDesktop} color="blue.600" ml={0} mr={1} mb={0} />
                                CapRover
                            </ListItem>
                        </ListContainer>
                    ]}
                    gh_link={"https://gitlab.com/crgcelestin/fisteriov2"}
                    video='https://www.youtube.com/embed/9d2VCgiliBM'
                >
                </ProjectCardItem >
            </Stack>
        </Flex>
    )
}
