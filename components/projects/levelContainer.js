import { useContext } from 'react';
import { LevelContext } from './level';
import {
    List
} from '@chakra-ui/react'

export const ListContainer = ({ children }) => {
    const level = useContext(LevelContext);
    return (
        <List>
            <LevelContext.Provider key={level + 1}>
                {children}
            </LevelContext.Provider>
        </List>
    )
}
