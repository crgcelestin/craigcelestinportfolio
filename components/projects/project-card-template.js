import {
    Box,
    Button,
    HStack,
    Icon,
    Image,
    LinkBox,
    Spacer,
    Stack,
    Tag,
    TagLabel,
    Text,
    VStack,
    AspectRatio,
    useColorModeValue
} from '@chakra-ui/react'
import NextLink from 'next/link'
import { FaGitlab } from 'react-icons/fa'
import { motion } from 'framer-motion'
import styled from '@emotion/styled'

const ContainerDiv = styled.div`
    position: relative;
    width: 100%;
    height: 0;
    padding-bottom: 80%;
`
const VideoDiv = styled.iframe`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
`

const variants = {
    hidden: { opacity: 0, x: 0, y: 20 },
    enter: { opacity: 1, x: 0, y: 0 },
    exit: { opacity: 0, x: -0, y: 20 }
}


export default function ProjectCardItem({ title, contributions, desc, video, icon, tags, gh_link, ...rest }) {
    const gitlab_border_colors = useColorModeValue('light', 'dark')
    const tagColors = useColorModeValue('gray.800', 'whiteAlpha.900')

    return (
        <motion.article
            initial="hidden"
            animate='enter'
            exit='exit'
            variants={variants}
            transition={{
                duration: 0.4,
                type: 'easeInOut'
            }}
            style={{
                position: 'relative'
            }}
        >
            <LinkBox
                as="article"
                {...rest}
                mt={3}
                bgColor={useColorModeValue('light', 'dark')}
            >
                <Stack
                    p={0}

                    overflow={"hidden"}
                    textAlign="left"
                    spacing={0}
                >
                    <Box
                        position="relative"
                        overflow={"hidden"}>
                        <HStack>
                            <Box mb={3}>
                                <HStack>
                                    <Image
                                        mt={-1}
                                        height="40px"
                                        width="40px"
                                        layout="fixed"
                                        src={icon}
                                        alt={title}
                                    ></Image>
                                    <Text my={1} fontWeight="bold" fontSize="2xl">
                                        {title}
                                    </Text>
                                </HStack>

                            </Box>
                            <Spacer></Spacer>
                            {gh_link && (
                                <Box>
                                    <VStack mt={-5}>
                                        <NextLink
                                            href={gh_link}
                                            target="_blank"
                                            rel="noopener noreferrer"
                                        >
                                            <Button
                                                mb={-7}
                                                variant="outline"
                                                alignItems="center"
                                                position={"relative"}
                                                cursor="pointer"
                                                borderRadius={"3px"}
                                                borderColor={gitlab_border_colors}
                                                textTransform={"uppercase"}
                                                padding={"0px 5px "}
                                                transition={"all .2s ease"}
                                                transition-timing-function="spring(4 100 10 10)"
                                                _hover={{
                                                    transform: "translateY(-2px)",
                                                    shadow: "lg",
                                                }}
                                            >
                                                <HStack>
                                                    <Icon as={FaGitlab} align={"center"} boxSize={"18px"} />
                                                </HStack>

                                            </Button>
                                        </NextLink>
                                    </VStack>
                                </Box>
                            )}
                        </HStack>
                        <Stack marginTop='10px' isInline overflow={"hidden"} overflowWrap={false}>
                            <Box mb={4} mt={0}>
                                {tags.map((tag) => (
                                    <Tag
                                        key={tag}
                                        borderRadius="sm"
                                        variant="outline"
                                        mr={2}
                                        mt="1"
                                    >
                                        <TagLabel>
                                            <Text color={tagColors} fontSize={"sm"}>
                                                {tag}
                                            </Text>
                                        </TagLabel>
                                    </Tag>
                                ))}
                            </Box>
                        </Stack>
                        <VStack align="start" justify="flex-start">

                            <HStack>
                                <Stack>
                                    <Text fontSize='md'>
                                        {desc}
                                    </Text>
                                    <Text>
                                        <strong>
                                            {'My Contributions '}
                                        </strong>
                                    </Text>
                                    {contributions}
                                    <ContainerDiv>
                                        <AspectRatio marginTop='20px' maxW="100%">

                                            <VideoDiv borderColor={useColorModeValue('gray.800', 'whiteAlpha.900')}
                                                width='720'
                                                height='405'
                                                src={video} title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share;fullscreen "></VideoDiv>
                                        </AspectRatio>
                                    </ContainerDiv>
                                </Stack>
                            </HStack>
                        </VStack>

                    </Box>
                </Stack>
            </LinkBox>
        </motion.article>
    );
}

ProjectCardItem.defaultProps = {
    gh_link: "",
    icon: "",
};
