import CurrentProjectsList from "./currentProjects";
import {
    Box,
    Heading,
    Stack
} from '@chakra-ui/react'
import Footer from "../footer";
import PastProjectsList from "./pastProjects";

export default function ProjectHeader() {
    return (
        <Stack>
            <Stack>
                <Box>
                    <Heading
                        my={"2"}
                        fontSize='3xl'
                        fontWeight='bold'
                        marginTop="20px"
                    >
                        {'Featured Projects'}
                    </Heading>
                </Box>
            </Stack>
            <Box>
                <CurrentProjectsList></CurrentProjectsList>
            </Box>
            <Footer></Footer>
            <Box>
                <PastProjectsList></PastProjectsList>
            </Box>
        </Stack>
    )
}
